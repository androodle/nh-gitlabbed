<?php

/**
 * 
 * @copyright 
 * @author excelsoft
 * @since
 *
 * file for validate alfresco properties with default values
 *
 * */
require('../../../config.php');
global $CFG, $USER;
require_once($CFG->dirroot . '/repository/lib.php');
$rid = $_POST['rid'];
$arr = array();
$final_data = array();
$prop_array = array();
$context = context_system::instance();
$errKeys = array();
//get the repository object 
$repo = repository::get_repository_by_id($rid, $context->id);
//get all the poperty default value array
for ($i = 0; $i <= $_POST['weightage'][0]; $i++) {
    if (!empty($_POST['property_name_' . $i])) {
        $aspectprop = str_replace(']', '', $_POST['property_name_' . $i]);
        $asparr = explode('[', $aspectprop);
        $prop = $asparr[1];
        $aspect = $asparr[0];
        if (empty($_POST['default_value_expression_' . $i])) {
            $expvalue = "testexp";
        } else {
            $expvalue = $_POST['default_value_expression_' . $i];
        }
        $arr[$i][$aspect][$prop] = $expvalue;
        for ($j = $i + 1; $j <= $_POST['weightage'][0]; $j++) {
            if (isset($_POST['property_name_' . $j])) {
            $aspectprop_inner = str_replace(']', '', $_POST['property_name_' . $j]);
            $asparr_inner = explode('[', $aspectprop_inner);
            $prop_inner = $asparr_inner[1];
            $aspect_inner = $asparr_inner[0];
            if ($aspect_inner == $aspect) {
                if (empty($_POST['default_value_expression_' . $j])) {
                    $expvalue = "testexp";
                } else {
                    $expvalue = $_POST['default_value_expression_' . $j];
                }
                $arr[$i][$aspect_inner][$prop_inner] = $expvalue;
                unset($_POST['property_name_' . $j]);
            }
        }
    }
}
}
foreach ($arr as $single_aspect) {
    foreach ($single_aspect as $key1 => $value1) {
        if (is_array($value1)) {
            foreach ($value1 as $key => $value) {
                $prop_array = explode(':', $key);
                if (count($prop_array) != 0) {
                    // Use key as it includes aspect prefix
                    $prop_values = $repo->fetch_search_values($key);
                    if (count($prop_values) != 0) {
                        foreach ($prop_values as $key2 => $value2) {
                            $single_aspect[$key1][$key] = $value2;
                        }
                    }
                }
            }
        }
    }
    if (!isset($data->uuid)) {
        try {
            $data = $repo->uploadToValidate($single_aspect, true);
            $final_data = array_merge($final_data, $data->properties);
        } catch (Exception $e) {
            array_push($errKeys, "ERROR");
        }
    } else {
        try {
            $data = $repo->uploadToValidate($single_aspect, false);
            $final_data = array_merge($final_data, $data->properties);
        } catch (Exception $e) {
            array_push($errKeys, "ERROR");
        }
    }
}
$data->properties = $final_data;
if (isset($data->uuid)) {
    $props = $data->properties;
    foreach ($arr as $key => $value) {
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    if (!isset($props[$key2]) && $key2 != 'cm:title' && $key2 != "cm:description" && $key2 != "cm:author") {
                        array_push($errKeys, $key2);
                    }
                }
            }
        }
    }
} else {
    array_push($errKeys, "ERROR");
}
if (isset($data->uuid)) {
    $obj_url = str_replace('s/cmis', 'service/cmis/i/', $repo->options['alfresco_cmis_url']);
    $node_id = str_replace('urn:uuid:', '', $data->uuid);
    $obj_url = $obj_url . $node_id;
    $repo->deleteNode($obj_url);
}
echo json_encode($errKeys);
?>
