/**
 * Function to Add a new row in Display Item Configuration Form.
 */
function addrRow(rid)
{
    var rowCount = $('#add tr').length;
    rowCount--;
    var str = "";
    for (var i = 0; i < 1; i++)
    {

        str += "<tr id='new_" + rowCount + "'  style='background-color:#E0E0E0;vertical-align:top;height:50px'>";
        str += "<td ><input type='hidden' name='weightage[]' id='weightage_" + rowCount + "' value='" + rowCount + "' >\n\
                 <input type='text' class='prop_name' name='property_name_" + rowCount + "' id='property_name_" + rowCount + "' ></td>";
        str += "<td ><input class='label_name' type='text' name='display_label_" + rowCount + "' id='display_label_" + rowCount + "'></td>";
        str += "<td ><select class='render_type' name='render_type_" + rowCount + "' id='render_type_" + rowCount + "' >";
        str += "<option value='Single Line Textfield'>Single Line Textfield</option>";
        str += "<option value='Multiline Text Box'>Multiline Text Box</option>";
        str += "</select>";
        str += "</td>";
        str += "<td ><img style='cursor:pointer' width='14' height='14' src='../pix/delete.png' onclick='fncDelRow(" + rowCount + ")'></td>";
        str += "</tr>";
    }
    $('#add > tbody:last').append(str);
    rowCount++;
    $("#add tbody").sortable("destroy");
    $("#add tbody").sortable({
        helper: fixHelper
    });
}

/**
 * FUNCTION to delete Row from the Display Item Configuration Form.
 */

function fncDelRow(id) {
    var rowCount = $('#add tr').length;
    $("#new_" + id).remove();
    rowCount--;
}

/**
 * Adjust the Width of the Form.
 */
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};


