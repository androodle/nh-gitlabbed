<?php

/**
 * Androgogic Course Rating
 * 
 * @author Eamon Delaney     
 * @version 2014031000    
 * @copyright Eamon Delaney  
 *
 **/

$plugin->version = 2014031800;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/course_rating/version.php
