<?php

/** 
 * Androgogic User Period Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the user_periods
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('andro_user_period',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_training_history'), 'notifysuccess');

?>
