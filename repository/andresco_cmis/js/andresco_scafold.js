/**
 * Function to Add a new row in Custom Property Configuration Form.
 */
function addrRow(rid)
{
    var rowCount = $('#add tr').length; 
    rowCount--;
    var str ="";        
    for(var i=0;i<1;i++)
    {           
            
        str+="<tr id='new_"+rowCount+"'  style='background-color:#E0E0E0;vertical-align:top;height:50px'>";
        str+="<td ><input type='hidden' name='weightage[]' id='weightage_"+rowCount+"' value='"+rowCount+"' >\n\
                 <input type='text' class='prop_name' name='property_name_"+rowCount+"' id='property_name_"+rowCount+"' ></td>";
        str+="<td ><input class='label_name' type='text' name='display_label_"+rowCount+"' id='display_label_"+rowCount+"'></td>";
        str+="<td ><input onclick='checkAlfresco("+rid+", "+rowCount+" )' type='checkbox' name='is_constraint_"+rowCount+"' id='is_constraint_"+rowCount+"'>\n\
                    </br><span style='color:red;' id='constraint_error_"+rowCount+"'></span></td>";
        str+="<td ><span id = 'default_span_"+rowCount+"'><input  onblur='getvalue(this)'  type='text' name='default_value_expression_"+rowCount+"' id='default_value_expression_"+rowCount+"'>\n\
                    <br/><span id='error_span_"+rowCount+"' style='color:red;'></span><span id='span_"+rowCount+"' style='color:green;'></span></span></td>";
        str+="<td ><select class='render_type' name='render_type_"+rowCount+"' id='render_type_"+rowCount+"' >";
        str+="<option value='Single Line Textfield'>Single Line Textfield</option>";
        str+="<option value='Do Not Render'>Do Not Render</option>";
        str+="<option value='Display as Read Only'>Display as Read Only</option>";
        str+="<option value='Multiline Textarea'>Multiline Textarea</option>";
        str+="<option value='Checkbox'>Checkbox</option>";
        str+="<option value='datetime'>Datetime</option>";        
        str+="</select>";
        str+="</td>";
        str+="<td ><input type='checkbox' name='is_mandatory_"+rowCount+"' id='is_mandatory_"+rowCount+"'></td>";
        str+="<td ><img style='cursor:pointer' width='14' height='14' src='../pix/delete.png' onclick='fncDelRow("+rowCount+")'></td>";
        str+="</tr>";
    }
    //$('#add').html(str); //or 
    $('#add > tbody:last').append(str);
    rowCount++;
    $("#add tbody").sortable("destroy");
    $("#add tbody").sortable({
        helper:fixHelper
    });
//$("#add tbody").disableSelection();
        
//$("#add").tableDnD();
}

/**
 * FUNCTION to delete Row from the Custom Property Configuration Form.
 */

function fncDelRow(id){
    var rowCount = $('#add tr').length; 
    $("#new_"+id).remove();
    rowCount--;
}

/**
 * Adjust the Width of the Form.
 */
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};
    
    
