<?php
/**
* Theme version info
*
* @package    theme
* @subpackage androtheme
* @copyright  Androgogic Pty Ltd
*
* Documentation: http://docs.moodle.org/dev/version.php
*/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015010900; // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100; // Requires this Moodle version
$plugin->component = 'theme_androtheme'; // Type and name of this plugin
$plugin->maturity = MATURITY_STABLE;
$plugin->release = 'v1.3';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2013050100,
);