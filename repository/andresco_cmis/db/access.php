<?php

/*-----------------------------------------------------------------------------*
 * Andresco Repository Plugin: Security
 *-----------------------------------------------------------------------------*
 *
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 *
 *-----------------------------------------------------------------------------*/

$capabilities = array(

    'repository/andresco_cmis:view' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'user' => CAP_ALLOW
        )
    )
);
