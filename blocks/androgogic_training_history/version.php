<?php
/** 
 * Androgogic Training History Block: Version
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$plugin->version = 2014080500;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/androgogic_training_history/version.php