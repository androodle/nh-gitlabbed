<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (empty($_POST)) {
    echo 'This is not the script you are looking for.';
    die();
}

// The moodle config file is required
require('../../../config.php');
$new_string = '';
$value_array = array();


$ANDRESCOFILE = new stdClass();
$ANDRESCOFILE->name = "Sample File";
$ANDRESCOFILE->type = "image/png";
$ANDRESCOFILE->size = 15726;

include_once('get_categoryinfo.php');
$ANDRESCOCOURSE = create_andrescourse();

global $COURSE, $ANDRESCO_FILE;

$flag = true;

if (isset($_POST['default_value']) && $_POST['default_value'] != '') {
    $default_value = $_POST['default_value'];
    //----Andresco: Begin----
    //Code To check for moodle context variables
    preg_match_all("/CV\W\S*\W/", $default_value, $match_array);
    foreach ($match_array[0] as $match) {
        $cvar = trim($match);
        $exp = substr($cvar, 4, -1);
        if (strpos($exp, '.')) {
            $arr_cxt = explode('.', $exp);
        }
        if (strpos($exp, '_')) {
            $arr_cxt = explode('_', $exp);
        }
        if (strpos($exp, '-')) {
            $arr_cxt = explode('-', $exp);
        }
        $key = strtoupper($arr_cxt[0]);
        if (array_key_exists($key, $GLOBALS)) {
            $obj = $GLOBALS[$key];
            if ((is_object($obj))) {
                if (isset($obj->$arr_cxt[1])) {
                    $value = $obj->$arr_cxt[1];
                    // @review This strips HTML tags, but there may be use cases in some CV[] fields where we don't 
                    // want to do this, case in point links (<a></a>) and images (<img></img>). These should
                    // be taken into consideration with enhancements to Andresco.
                    $value = strip_tags(trim($value));                    
                    $default_value = str_replace($match, ' ' . $value . ' ', $default_value);
                    $flag = true;
                } else {
                    $value = str_replace(array('CV[', 'cv[', ']'), '', $cvar) . ' doesnt exist';
                    $flag = false;
                    break;
                }
            } else {
                $value = $key . ' doesnt exist';
                $flag = false;
                break;
            }
        } else {
            $value = '$' . $key . ' doesnt exist';
            $flag = false;
            break;
        }
    }
    if ($flag) {
        echo $success_data = json_encode(array('result' => 'Successful', 'value' => $default_value), JSON_FORCE_OBJECT);
        die;
    } else {
        echo $success_data = json_encode(array('result' => 'Unsuccessful', 'value' => $value), JSON_FORCE_OBJECT);
        die;
    }
} else {
    echo $success_data = json_encode(array('result' => 'Unsuccessful', 'value' => ''), JSON_FORCE_OBJECT);
    die;
}
